package ru.kuzin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.command.AbstractCommand;
import ru.kuzin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}