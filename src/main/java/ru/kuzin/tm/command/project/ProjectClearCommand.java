package ru.kuzin.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
        getProjectService().clear(userId);
    }

}