package ru.kuzin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.repository.IUserRepository;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.model.User;
import ru.kuzin.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    public User create(@NotNull final IPropertyService propertyService, @NotNull final String login, @NotNull final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    public User create(@NotNull final IPropertyService propertyService, @NotNull final String login, @NotNull final String password, @Nullable final String email) {
        final User user = create(propertyService, login, password);
        user.setEmail(email);
        return user;
    }

    @Nullable
    public User create(@NotNull final IPropertyService propertyService, @NotNull final String login, @NotNull final String password, @Nullable final Role role) {
        final User user = create(propertyService, login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return findAll()
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return findAll()
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findAll()
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findAll()
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}